
import argparse
from algo.btcmarkets import main as btcmarkets
from algo.market_maker import main as market_maker
from algo.util import get_log_file_handle, logger


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('--script', help='The script you want to run')
    parser.add_argument('--log_file')
    args = parser.parse_args()

    handle = get_log_file_handle(args.log_file)
    logger.addHandler(handle)

    if args.script == 'market_maker':
        market_maker()
    elif args.script == 'btcmarkets':
        btcmarkets()
