
import os
import sys
import time
import json
import hmac
import base64
import hashlib
import collections
import zmq
import logging


LOG_FORMAT = '%(asctime)s - %(levelname)s - %(module)s - %(funcName)s - %(message)s'
logging.basicConfig(
    level=logging.DEBUG,
    handlers=[
        logging.StreamHandler(sys.stdout)
    ],
    format=LOG_FORMAT,
)

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


def build_headers(end_point, post_data=None):

    api_key = os.environ['BTCMARKETS_API_KEY'].encode("utf-8")
    secret = os.environ['BTCMARKETS_SECRET'].encode("utf-8")

    timestamp = str(int(time.time() * 1000))
    sbody = end_point + "\n" + timestamp + "\n"
    if post_data is not None:
        sbody += post_data
    rsig = hmac.new(base64.standard_b64decode(secret), sbody.encode("utf-8"), hashlib.sha512)
    bsig = base64.standard_b64encode(rsig.digest()).decode("utf-8")

    return collections.OrderedDict([
        ("Accept", "application/json"),
        ("Accept-Charset", "UTF-8"),
        ("Content-Type", "application/json"),
        ("apikey", api_key),
        ("timestamp", timestamp),
        ("signature", bsig),
    ])


def publisher(address="tcp://127.0.0.1:5556"):
    """ to publish you need a publisher
    >>> publish = publisher()
    >>> publish('a/topic', 'a message I want to broadcast')
    """
    context = zmq.Context()
    socket = context.socket(zmq.PUB)
    socket.bind(address)

    def publish(topic, message):
        """
        publishes a message on a topic
        """
        if not isinstance(message, str):
            message = json.dumps(message)
        logger.info('%s %s' % (topic, message))
        socket.send_string("%s %s" % (topic, message))
    return publish


def subscribe(*topics, address="tcp://127.0.0.1:5556"):
    """
    >>> messages = subscribe('a/topic', 'another/topic')
    >>> for message in messages:
    ...     # do something
    """
    context = zmq.Context()
    socket = context.socket(zmq.SUB)
    socket.connect(address)
    for topic in topics:
        socket.setsockopt_string(zmq.SUBSCRIBE, topic)
    while True:
        if socket.poll(timeout=50) is not 0:
            raw = socket.recv()
            topic, msg = raw.decode().split(' ', maxsplit=1)
            yield topic, json.loads(msg)
        else:
            yield None


def get_log_file_handle(filename):
    from logging.handlers import TimedRotatingFileHandler
    handle = TimedRotatingFileHandler(
        filename=filename,
        when='midnight',
        interval=1,
        backupCount=10000,
        utc=True,
    )
    handle.setFormatter(logging.Formatter(LOG_FORMAT))
    return handle