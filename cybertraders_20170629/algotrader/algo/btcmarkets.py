
import json
import time
from .btcmarkets_api import BTCMarketsAPI
from .util import publisher, subscribe, logger


INSTRUMENT = 'BTC'
CURRENCY = 'AUD'

INCOMING_ADDR = "tcp://127.0.0.1:5556"
INCOMING_CHANNELS = ('insert_order', 'delete_order', 'delete_open_orders', 'get_account')
OUTGOING_ADDR = "tcp://127.0.0.1:5557"
OUTGOING_CHANNELS = (
    'account_update', 'order_book_update', 'order_insert_update', 'order_delete_update',
    'position_update', 'open_orders_update', 'trade_update', 'live_order_update',
)


class BTCM:

    def __init__(self):
        self.api = BTCMarketsAPI(INSTRUMENT, CURRENCY)
        self.publish = publisher(OUTGOING_ADDR)
        self.subscriptions = {channel: subscribe(channel, address=INCOMING_ADDR) for channel in INCOMING_CHANNELS}
        self.last_updates = {source: 0 for source in ('open_orders', 'order_history', 'trade_history', 'order_book')}
        self.live_orders = {}

    def get_account_update(self):
        account_update = self.api.get_accounts()
        return {
            acc['currency']: acc['balance'] / self.api.PRICE_MULTI
            for acc in account_update if acc['currency'] in (self.api.instrument, self.api.currency)
        }

    def on_get_account(self):
        msg = next(self.subscriptions['get_account'])
        if msg is not None:
            account_update = self.get_account_update()
            self.publish('account_update', account_update)
            logger.info(account_update)

    def on_order_insert(self):
        for raw in self.subscriptions['insert_order']:
            if raw is None:
                return
            topic, msg = raw
            resp = self.api.insert_order(**msg)
            self.publish('order_insert_update', resp)
            self.live_orders[resp['id']] = resp
            logger.info(json.dumps(raw))
            logger.info(json.dumps(resp))

    def on_order_delete(self):
        for raw in self.subscriptions['delete_order']:
            if raw is None:
                return
            topic, msg = raw
            resp = self.api.delete_order(order_ids=msg)
            self.publish('order_delete_update', resp)
            logger.info(json.dumps(raw))
            logger.info(json.dumps(resp))

    def on_delete_open_orders(self):
        for raw in self.subscriptions['delete_open_orders']:
            if raw is None:
                return
            order_ids = self.api.get_open_orders()['orders']
            logger.info('Deleting open orders %s' % str(order_ids))
            if order_ids:
                resp = self.api.delete_order(order_ids=[order['id'] for order in order_ids])
                self.publish('order_delete_update', resp)
                logger.info(json.dumps(raw))
                logger.info(json.dumps(resp))

    def update_open_orders(self):
        if self.live_orders:
            open_orders = self.api.get_order_detail(order_ids=list(self.live_orders.keys()))['orders']
            positions = self.get_account_update()
            self.publish('open_orders_update', open_orders)
            self.publish('position_update', positions)
            logger.info(json.dumps(open_orders))
            logger.info(json.dumps(positions))

    def update_trade_history(self):
        trade_history = self.api.get_trade_history(since=self.last_updates['trade_history'])['trades']
        if trade_history:
            self.last_updates['trade_history'] = max([trade['id'] for trade in trade_history]) + 1
            self.publish('trade_update', trade_history)
            logger.info(json.dumps(trade_history))

    def update_market_data(self):
        order_book = self.api.get_order_book()
        if order_book != self.last_updates['order_book']:
            self.last_updates['order_book'] = order_book
            self.publish('order_book_update', order_book)
            logger.info(json.dumps(order_book))

    def update_live_orders(self):
        if not self.live_orders:
            return
        resp = self.api.get_order_detail(order_ids=list(self.live_orders.keys()))
        for order in [order for order in resp['orders'] if order['status'] in ('Placed', 'Fully Matched', )]:
            if order == self.live_orders[order['id']]:
                continue
            order['price'] /= self.api.PRICE_MULTI
            order['volume'] /= self.api.VOLUME_MULTI
            if order['status'] == 'Placed':
                self.publish('live_order_update', order)
                self.live_orders[order['id']] = order
            elif order['status'] == 'Fully Matched':
                self.publish('order_traded', order)
                del self.live_orders[order['id']]

    def start(self):
        throttle_seconds = 2.0
        last_run = time.time()

        while True:
            self.on_get_account()
            self.on_order_insert()
            self.on_order_delete()
            self.on_delete_open_orders()
            # self.update_open_orders()
            # self.update_trade_history()
            self.update_market_data()
            self.update_live_orders()

            # Throttle HTTP calls
            now = time.time()
            run_time = now - last_run
            if run_time < throttle_seconds:
                time.sleep(throttle_seconds - run_time)
                now = time.time()
            last_run = now


def main():
    import logging
    logging.getLogger('requests').setLevel(logging.INFO)
    btc = BTCM()
    btc.start()


if __name__ == '__main__':
    main()