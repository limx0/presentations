
from algo.btcmarkets import OUTGOING_CHANNELS, INSTRUMENT, CURRENCY, INCOMING_ADDR as OUTGOING_ADDR, OUTGOING_ADDR as INCOMING_ADDR
from algo.util import subscribe, publisher, logger


class MarketMaker:

    def __init__(self, config, quote_percentage, quote_size, price_change_theo, levels=5):

        # Config
        self.config = config
        self.quote_percentage = quote_percentage
        self.quote_size = quote_size
        self.levels = levels
        self.retreat_per_position = self.quote_percentage / levels,
        self.max_position = self.quote_size * 5.,
        self.price_change_theo = price_change_theo

        # Calculation and Trading
        self.theoretical = None
        self.mid_point = None
        self.quote_theoretical = None
        self.order_book = None
        self.position = None
        self.position_level = 0
        self.quotes = {}
        self.trades = []

        # Publishing and state
        self.publish = publisher(address=OUTGOING_ADDR)
        self.initialised = False
        self.awaiting_confirmation = False

    def on_account_update(self, positions):
        self.position = (positions[INSTRUMENT] - self.config['base_btc'])

    def update_order_book(self, order_book):
        self.order_book = order_book

    def update_theoretical(self):
        if self.order_book is None:
            return
        # Remove our orders from the orderbook when calculating our theo
        quote_bids = [q['price'] for q in self.quotes.values() if q.get('orderSide') == 'Bid']
        bids = [b for b in self.order_book['bids'] if b[0] not in quote_bids]
        quote_asks = [q['price'] for q in self.quotes.values() if q.get('orderSide') == 'Ask']
        asks = [a for a in self.order_book['asks'] if a[0] not in quote_asks]

        self.mid_point = (bids[0][0] + asks[0][0]) / 2.
        self.theoretical = self.mid_point + (self.position_level * self.quote_percentage * self.mid_point)
        logger.info('midpoint: %s, theo: %s, quote_theo: %s' % (self.mid_point, self.theoretical, self.quote_theoretical))

    def on_order_insert_update(self, order_update):
        self.quotes[order_update['id']] = order_update
        if len(self.quotes.keys()) == 2:
            # We've received confirmation of both our quotes
            self.awaiting_confirmation = False
        logger.info(self.quotes)

    def on_order_delete_update(self, order_update):
        for order in order_update['responses']:
            if order['id'] in self.quotes:
                del self.quotes[order['id']]
        logger.info(self.quotes)

    def update_position(self, account_update):
        # Normally this would be done by summing our trades or querying positions,
        # but in BTCMarkets our position is kept as an account balance.
        self.on_account_update(account_update)
        self.position_level = (self.position - (self.position % self.quote_size)) / self.quote_size
        logger.info('position: %s, position_level: %s' % (self.position, self.position_level))

    def on_order_traded(self, message):
        self.trades.append(message)

    def generate_quotes(self):

        if self.awaiting_confirmation:
            # We have live orders in flight
            return

        elif self.quote_theoretical is not None and (abs(self.theoretical - self.quote_theoretical) < self.price_change_theo):
            # We haven't moved enough to adjust out quotes yet
            return

        else:
            # We need to update our quotes

            # Delete any orders we currently have in the market
            if self.quotes:
                self.publish('delete_order', list(self.quotes.keys()))

            # Place a bid order if we haven't yet reached our max position
            if self.position_level < 5:
                bid_price = self.theoretical - (self.theoretical * self.quote_percentage)
                order_data = dict(
                    instrument=INSTRUMENT, currency=CURRENCY, order_side='Bid',
                    price=bid_price, volume=self.quote_size, order_type='Limit'
                )
                self.publish('insert_order', order_data)
                self.awaiting_confirmation = True

            # Place an ask order if we haven't yet reached our max position
            if self.position_level > -5:
                # Place our offer
                ask_price = self.theoretical + (self.theoretical * self.quote_percentage)
                order_data = dict(
                    instrument=INSTRUMENT, currency=CURRENCY, order_side='Ask',
                    price=ask_price, volume=self.quote_size, order_type='Limit'
                )
                self.publish('insert_order', order_data)
                self.awaiting_confirmation = True

            self.quote_theoretical = self.theoretical

    def publish_state(self):
        import json
        if self.order_book is not None and self.theoretical is not None:
            state = {
                'midpoint': self.mid_point,
                'theoretical': self.theoretical,
                'quotes': self.quotes,
                'position': self.position,
                'position_level': self.position_level,
                'best_bid': self.order_book['bids'][0][0],
                'best_ask': self.order_book['asks'][0][0],
                'trades': self.trades
            }
            logger.info(json.dumps(state))
            self.trades = []

    def start(self):

        market_messages = filter(None, subscribe(*OUTGOING_CHANNELS, address=INCOMING_ADDR))

        for topic, message in market_messages:
            logger.info('%s - %s' % (topic, message))
            if not self.initialised and topic != 'account_update':
                # Don't start until we have an account message
                self.publish('get_account', {})
                self.publish('delete_open_orders', {})
                continue
            if topic == 'account_update':
                self.on_account_update(message)
                if not self.initialised:
                    self.initialised = True
                    logger.info('Initialised - %s' % self.position)
            elif topic == 'order_book_update':
                self.update_order_book(message)
                self.update_theoretical()
                self.generate_quotes()
            elif topic == 'order_insert_update':
                self.on_order_insert_update(message)
            elif topic == 'order_delete_update':
                self.on_order_delete_update(message)
            elif topic == 'position_update':
                self.update_position(message)
                self.update_theoretical()
            elif topic == 'live_order_update':
                self.on_order_insert_update(message)
            elif topic == 'order_traded':
                self.on_order_traded(message)
            else:
                print('ERR', topic, message)

            self.publish_state()


def main():

    config = {
        'base_btc': 0.02,
        'quote_move_theo': 0.5,
    }

    mm = MarketMaker(
        config=config,
        quote_percentage=0.0075,
        quote_size=0.005,
        price_change_theo=2.5,
    )
    # Clear our current orders
    mm.publish('delete_open_orders', {})
    mm.start()


if __name__ == '__main__':
    main()
