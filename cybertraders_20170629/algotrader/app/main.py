
import os
import json
import datetime
import pathlib
from tornado.ioloop import IOLoop
from bokeh.application.handlers import FunctionHandler
from bokeh.application import Application
from bokeh.layouts import row
from bokeh.plotting import figure
from bokeh.server.server import Server
from bokeh.layouts import gridplot
from bokeh.models import ColumnDataSource
from bokeh.driving import count


file_handle = pathlib.Path(os.environ['DROPBOX'], 'testing', 'market_maker.log').open()

market_lines = {
    'best_bid': '#B2DF8A',
    'best_ask': '#FB9A99',
    'midpoint': '#A6CEE3',
    'theoretical': '#33A02C',
    'timestamp': None,
}

position_lines = {'timestamp': None, 'position': 'Blue'}


def main(doc):
    tools = "xpan,xwheel_zoom,xbox_zoom,reset"

    p1 = figure(title='BTCMarkets', x_axis_type="datetime", plot_height=500, plot_width=700,
                tools=tools, y_axis_location="right")
    p2 = figure(title="Position", x_axis_type="datetime", plot_height=300, plot_width=700,
                tools=tools, y_axis_location="right")
    mkt_source = ColumnDataSource(dict(timestamp=[], best_bid=[], best_ask=[], midpoint=[], theoretical=[]))
    pos_source = ColumnDataSource(dict(timestamp=[], position=[]))

    for key, col in market_lines.items():
        if key == 'timestamp':
            continue
        p1.line(x='timestamp', y=key, color=col, source=mkt_source)

    for key, col in position_lines.items():
        if key == 'timestamp':
            continue
        p2.line(x='timestamp', y=key, color=col, source=pos_source)

    @count()
    def update(t):
        def parse_line(line):
            timestamp, *_, raw = line.split(' - ', maxsplit=4)
            return dict(
                **{'timestamp': datetime.datetime.strptime(timestamp, '%Y-%m-%d %H:%M:%S,%f')},
                **json.loads(raw)
            )

        line = file_handle.readline()
        if 'publish_state' not in line:
            return

        new_data = parse_line(line)
        market_data = {k: [new_data[k]] for k in market_lines}
        position_data = {k: [new_data[k]] for k in position_lines}

        mkt_source.stream(market_data, 300)
        pos_source.stream(position_data, 300)

    doc.add_root(row(gridplot([[p1], [p2]], toolbar_location="left", plot_width=1000)))
    doc.add_periodic_callback(update, 50)
    doc.title = "BTC Market Maker"


if __name__ == '__main__':

    io_loop = IOLoop.current()
    bokeh_app = Application(FunctionHandler(main))

    server = Server({'/': bokeh_app}, io_loop=io_loop)
    server.start()

    print('Opening Bokeh application on http://localhost:5006/')
    io_loop.add_callback(server.show, "/")
    io_loop.start()
