import logging.config

import websocket

logging.config.fileConfig('logging.conf')
logger = logging.getLogger('simpleExample')

PAIRS = [
    'tBTCUSD',
    # 'tETHUSD',
    # 'tXRPUSD',
    # 'tLTCUSD',
    # 'tEOSUSD',
    # 'tNEOUSD',
]


def subscribe(self):
    for pair in PAIRS:
        self.send('{ "event": "subscribe", "channel": "book", "symbol": "%s"}' % pair)
        # self.send('{ "event": "subscribe", "channel": "trades", "symbol": "%s"}' % pair)


def log_message(self, evt):
    logger.info(evt)


def create_websocket():
    ws = websocket.WebSocketApp('wss://api-pub.bitfinex.com/ws/2')
    ws.on_open = subscribe
    ws.on_message = log_message
    return ws


if __name__ == '__main__':
    ws = create_websocket()
    ws.run_forever()
