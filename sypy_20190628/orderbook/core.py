import datetime
import json
import logging
import re
from itertools import islice

from frozendict import FrozenOrderedDict
from sortedcontainers import SortedDict
from streamz import Stream, from_textfile

CHANNELS = {}


def re_log_line(line):
    re_timestamp = '\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2},\d{3}'
    re_level = 'DEBUG|INFO|WARN|ERROR'
    matches = re.findall(f'({re_timestamp}) - ({re_level}) - (.*)\n', line)[0]
    return dict(zip(['timestamp', 'level', 'msg'], matches))


def parse_timestamp(s):
    return datetime.datetime.strptime(s, '%Y-%m-%d %H:%M:%S,%f')


def parse_orderbook_result(data):
    if not isinstance(data[0], list):
        data = [data]
    return [dict(zip(['price', 'count', 'amount'], x)) for x in data]


def parse_json(d):
    msg = json.loads(d['msg'])
    timestamp = parse_timestamp(d['timestamp'])
    if isinstance(msg, dict) and msg.get('event', None) == 'subscribed':
        CHANNELS[msg['chanId']] = {'pair': msg['pair'], 'type': msg['channel']}
    elif isinstance(msg, list):
        if msg[1] == 'hb':
            return
        channel_id, result = msg
        channel = CHANNELS[channel_id]
        data = parse_orderbook_result(result)
        return [{**{'channel': channel['type'], 'pair': channel['pair'], 'timestamp': timestamp}, **d} for d in data]

    logging.warning('Did not process %s' % str(d))


@Stream.register_api()
class orderbook(Stream):
    def __init__(self, upstream, num_levels=3):
        self.book = {'bids': SortedDict(), 'asks': SortedDict()}
        self.num_levels = num_levels
        super().__init__(upstream, asynchronous=False)

    def _top(self):
        bids = list(islice(reversed(self.book['bids'].values()), self.num_levels))
        asks = list(islice(self.book['asks'].values(), self.num_levels))
        timestamp = max(x['timestamp'] for x in bids + asks)
        return [
            FrozenOrderedDict(
                zip(('timestamp', 'side', 'price', 'size'), (timestamp, x['side'], x['price'], abs(x['amount'])))
            ) for x in bids + asks]

    def update(self, x, who=None):
        print(x)
        x['side'] = 'bids' if x['amount'] > 0 else 'asks'
        if x['count'] == 0:
            self.book[x['side']].pop(x['price'], None)
        else:
            self.book[x['side']][x['price']] = x
        self._emit(self._top())


if __name__ == '__main__':
    from asyncio import get_event_loop
    loop = get_event_loop()

    log_file = '../info_feed/bitfinex_info.log'
    log_lines = from_textfile(f=log_file, loop=loop)
    lines = log_lines.map(re_log_line)
    data = lines.map(parse_json)
    stream = data.filter(lambda x: x is not None).flatten()
    ob = stream.orderbook(num_levels=3).flatten().unique()
    ob.sink(print)

    log_lines.start()

    loop.run_forever()
