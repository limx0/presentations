import pytest
from streamz import from_textfile

from sypy_20190628.orderbook.core import re_log_line, parse_json


async def test_orderbook():
    log_file = '../info_feed/bitfinex_info.log'
    log_lines = from_textfile(f=log_file, asynchronous=False)
    lines = log_lines.map(re_log_line)
    data = lines.map(parse_json)
    stream = data.filter(lambda x: x is not None).flatten()
    ob = stream.orderbook(num_levels=3).flatten().unique()
    ob.sink(print)

    log_lines.start()

    import asyncio
    await asyncio.sleep(1e9)
